//
//  AddViewController.h
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoTableViewController.h"
#import "ToDo.h"

@interface AddViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *toDos;
@end
