//
//  ToDoTableViewController.h
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDo.h"
#import "InfoViewController.h"
#import "AddViewController.h"

@interface ToDoTableViewController : UITableViewController

@property (strong,nonatomic)NSMutableArray *toDos;
-(void)load;
-(void)save;
@end
