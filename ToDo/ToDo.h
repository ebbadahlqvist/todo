//
//  ToDo.h
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDo : NSObject
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* info;

-(instancetype)initWithName:(NSString*) name andInfo: (NSString*)info;
-(id)initWithCoder:(NSCoder *)decoder;
-(void)encodeWithCoder:(NSCoder *)encoder;


@end
