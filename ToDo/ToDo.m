//
//  ToDo.m
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ToDo.h"

@implementation ToDo

-(instancetype)initWithName:(NSString*) name andInfo: (NSString*)info{

    self = [super init];
    
    if(self){
        _name = name;
        _info = info;
    
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.info forKey:@"info"];
    
}

-(id)initWithCoder:(NSCoder *)decoder {
    
    self = [super init];
    
    if(self){
        self.name = [decoder decodeObjectForKey:@"name"];
        self.info = [decoder decodeObjectForKey:@"info"];
    }
    
    return self;
    
}


@end
