//
//  InfoViewController.h
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController
@property (nonatomic)NSString *name;
@property (nonatomic)NSString *info;

@end
