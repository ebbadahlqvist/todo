//
//  ToDoTableViewController.m
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ToDoTableViewController.h"

@interface ToDoTableViewController ()
@property (weak, nonatomic) IBOutlet UIRefreshControl *refresh;

@end

@implementation ToDoTableViewController
- (NSMutableArray*) toDos
{
    if (!_toDos){
        _toDos = [[NSMutableArray alloc] init];
        [self addToDosToMutableArray];
    }
    return _toDos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
   
   }


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.toDos.count;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToDoCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.toDos[indexPath.row] name];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.toDos removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"CellClick"]) {
        InfoViewController *destination = [segue destinationViewController];
        UITableViewCell *cell = sender;
        int row = (int)[self.tableView indexPathForCell:cell].row;
        destination.title =[self.toDos[row] name];
        destination.info = [self.toDos [row] info];
        destination.name = [self.toDos [row] name];
        
        
    } else if ([segue.identifier isEqualToString:@"AddSegue"]){
        AddViewController *modalAdd = (AddViewController*)segue.destinationViewController;
        modalAdd.toDos = self.toDos;

        
    }else{
    
    
    }
   
}
- (IBAction)refresh:(UIRefreshControl *)sender {
    
    [self.tableView reloadData];
    [sender endRefreshing];
}
-(void)save{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:self.toDos];
    [defaults setObject:myData forKey:@"myData"];
    [defaults synchronize];
}


-(void)load {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *myData = [defaults objectForKey:@"myData" ];
    if(myData != nil){
        self.toDos = [NSKeyedUnarchiver unarchiveObjectWithData:myData];
    }
    
    
}


-(void)addToDosToMutableArray{
    
    ToDo* one = [[ToDo alloc]initWithName:@"Tvätta" andInfo:@"Snarast!"];
    ToDo* two = [[ToDo alloc]initWithName:@"Städa" andInfo:@"Denna veckan"];
    ToDo* three = [[ToDo alloc]initWithName:@"Plugga" andInfo:@"Låg prioritet!"];
    
    [self.toDos addObject:one];
    [self.toDos addObject:two];
    [self.toDos addObject:three];
    
}



@end
