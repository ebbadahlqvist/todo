//
//  AddViewController.m
//  ToDo
//
//  Created by Ebba on 2016-02-09.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "AddViewController.h"
#import "ToDoTableViewController.h"



@interface AddViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *infoText;

@end

@implementation AddViewController


- (void)viewDidLoad {
    [super viewDidLoad];
       
    // Do any additional setup after loading the view.
}
- (IBAction)addButton:(UIButton*)sender {
    
    NSString *name =[self.nameText text];
    NSString *info =[self.infoText text];
    ToDo *newToDo = [[ToDo alloc]initWithName:name andInfo:info];
    NSLog(@"%@",name);
    [self.toDos addObject:newToDo];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
